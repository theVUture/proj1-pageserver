
  ```
  ## Author: Vu Vo,  vuv@uoregon.edu ##
  ```
  
*
  ```
  git clone <yourGitRepository> <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ```
  